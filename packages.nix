with (import <nixpkgs> {
  overlays = [ (import ./overlay.nix ) ];
}); {
  inherit gn;
  inherit libwebrtc libwebrtc_dmm_debug libwebrtc_dmm_release;
}
