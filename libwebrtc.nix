{
  # compilers
  llvmPackages_5,

  # build tools
  fetchurl, ninja, gn, pkgconfig, python,

  # build deps
  glib, gnome3, alsaLib, libpulseaudio, xorg,

  llvmPackage ? llvmPackages_5,

  # TODO: can override merge with this without the seperate "extraGnArgs" ?
  gnArgs ? {
    is_clang                   = llvmPackage.stdenv.cc.isClang;
    clang_base_path            = "${llvmPackage.stdenv.cc}";
    clang_use_chrome_plugins   = false;
    use_sysroot                = false;
    linux_use_bundled_binutils = false;

    # Needed to build "patched yasm"
    treat_warnings_as_errors = false;
  },

  extraGnArgs ? {},
}:
let stdenv = llvmPackage.stdenv;
    llvm = llvmPackage.llvm;
    formatValue = v: if builtins.isString v then "\\\"${v}\\\""
                     else if builtins.isBool v then (if v then "true" else "false")
                     else builtins.toString v;
    formatArgs = as: stdenv.lib.concatStringsSep " " (
                       stdenv.lib.mapAttrsToList (n: v: "${n}=${formatValue v}") as);
in stdenv.mkDerivation rec {
  name = "libwebrtc-59";
  src = fetchurl {
    url = "https://s3-ap-northeast-1.amazonaws.com/nix-misc.cons.org.nz/webrtc-59-a38fe22f.tar.xz";
    sha256 = "0vrm31hl5kiplp1280sfn75xv9fc9hk90v1x7zdh6pjyihz29qlh";
  };

  outputs = [ "out" "dev" "bin" ];

  nativeBuildInputs = [
    llvm python pkgconfig gn ninja
  ];
  buildInputs = [
    glib gnome3.gtk
  ] ++ stdenv.lib.optionals stdenv.isLinux [ alsaLib libpulseaudio.dev xorg.libXtst ];

  postPatch = ''
    rm -rf third_party/binutils
    substituteInPlace build/toolchain/gcc_toolchain.gni \
      --replace "\''${prefix}/llvm-ar" "${llvm}/bin/llvm-ar"
  '';

  gnCmd = ''
    gn gen out/build --args="${formatArgs (gnArgs // extraGnArgs)}"
  '';

  buildPhase = ''
    ${gnCmd}
    ninja -C out/build
  '';

  installPhase = ''
    mkdir -p ''${!outputBin}/bin/
    find out/build -maxdepth 1 -type f -perm /111 -print0 | xargs -0 -i cp {} ''${!outputBin}/bin/

    mkdir -p ''${!outputLib}/lib/
    cp \
      out/build/obj/webrtc/libwebrtc.a \
      out/build/obj/webrtc/libwebrtc_common.a \
      ''${!outputLib}/lib/

    mkdir -p ''${!outputDev}/include/
    tar --create --null --verbatim-files-from --files-from <(find webrtc -type f -name '*.h' -print0) \
      | tar --extract -C ''${!outputDev}/include/
  '';
}

