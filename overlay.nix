let dmmDefaultArgs = {
  use_rtti           = true;
  ffmpeg_branding    = "Chrome";
  proprietary_codecs = true;
};
in self: super: {
    gn = super.callPackage ./gn.nix {
      inherit (self.darwin) apple_sdk libobjc cf-private cctools;
    };

    libwebrtc  = super.callPackage ./libwebrtc.nix {};

    libwebrtc_dmm_debug = self.libwebrtc.override {
      extraGnArgs = dmmDefaultArgs // { is_debug = true; };
    };

    libwebrtc_dmm_release = self.libwebrtc.override {
      extraGnArgs = dmmDefaultArgs // { is_debug = false; };
    };
}
