{ stdenv, lib, fetchurl, libevent, ninja, python, apple_sdk, libobjc, cf-private, cctools }:

  stdenv.mkDerivation rec {
    name = "gn";
    version = "59";
    src = fetchurl {
      url = "https://s3-ap-northeast-1.amazonaws.com/nix-misc.cons.org.nz/webrtc-59-a38fe22f.tar.xz";
      sha256 = "0vrm31hl5kiplp1280sfn75xv9fc9hk90v1x7zdh6pjyihz29qlh";
    };

    postPatch = ''
      # Patch shebands (for sandbox build)
      patchShebangs build

      # Patch out Chromium-bundled libevent
      sed -i -e '/static_libraries.*libevent/,/^ *\]\?[})]$/d' \
          tools/gn/bootstrap/bootstrap.py
    '';

    NIX_LDFLAGS = "-levent";

    nativeBuildInputs = [ ninja python ]
      ++ lib.optional stdenv.isDarwin cctools;

    buildInputs = [ libevent ]
      ++ lib.optionals stdenv.isDarwin (
           with apple_sdk.frameworks; [
             # the order of these is significant
             cf-private
             CoreFoundation
             Foundation
             ApplicationServices
             AppKit
           ]
      );

    buildPhase = ''
      python tools/gn/bootstrap/bootstrap.py -s
    '';

    installPhase = ''
      install -vD out/Release/gn "$out/bin/gn"
    '';

    meta = with stdenv.lib; {
      description = "A meta-build system that generates NinjaBuild files";
      homepage = https://chromium.googlesource.com/chromium/src/tools/gn;
      license = licenses.bsd3;
    };
  }
